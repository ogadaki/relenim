import Vue from 'vue'
import App from './App.vue'

import Trend from "vuetrend"
Vue.use(Trend)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
